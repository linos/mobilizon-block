import { request } from 'graphql-request';


// export function addHttpsProtocolIfNeeded (baseURL) {
//     if (!baseURL.startsWith("https://")) {
//         var baseURL = "https://" + baseURL;
//     }
//     return baseURL
// }


export async function isMobilizonInstance(baseURL) {
    try {
        let response = await fetch(baseURL + '/.well-known/nodeinfo/2.1');
        if (response.ok) {
            let json = await response.json();
            if (json.software.name === "Mobilizon") {
                return true;
            }
            else {
                return false;
            }
        }
    } catch (error) {
        return false;
    }
}

export async function groupExists(baseURL, groupName) {
    const url = baseURL + "/api";
    const query = `
        query ($groupName: String!) {
            group(preferredUsername: $groupName) {
                type
            }
        }
        `;
    try {
        await request(url, query, { groupName });
        // if the group is not found the response looks like:
        // {
        //     "data": {
        //       "group": null
        //     },
        //     "errors": [
        //       {
        //         "code": "group_not_found",
        //         "field": null,
        //         "locations": [
        //           {
        //             "column": 3,
        //             "line": 7
        //           }
        //         ],
        //         "message": "Group not found",
        //         "path": [
        //           "group"
        //         ],
        //         "status_code": 404
        //       }
        //     ]
        //   }
        //
        // So if we didn't catch an error, true can be returned
        return true;
    } catch (error) {
        return false;
    }
}